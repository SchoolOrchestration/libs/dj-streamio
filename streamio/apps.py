from django.apps import AppConfig


class StreamioConfig(AppConfig):
    name = 'streamio'

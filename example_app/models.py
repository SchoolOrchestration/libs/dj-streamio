from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from streamio.mixins import StreamModelMixin
from rest_framework import serializers

TODO_STATUSES = [
    ('N', 'Todo'),
    ('P', 'In Progress'),
    ('D', 'Done'),
]

def test_task(*args, **kwargs):
    print(args)
    print(kwargs)


class Todo(StreamModelMixin, models.Model):

    collection = 'todos'
    feed_name = 'todo'
    feed_actor_field = 'owner_id'
    feed_once_off_actions = ["create"]
    feed_related_mapping = [
        # feed_slug, model_field
        ('user', 'owner_id'),
        ('user', 'users'),
        ('todo', 'id'),
    ]
    # a list of object_id prefixes to look for in the `object_ids` field
    feed_object_ids_mapping = [
        # feed_slug, object_id collection
        ('user', 'user'),
        ('notification', 'notification'),
        ('todo', 'foo'),
    ]
    enrichment_serializer = 'example_app.models.TodoSerializer'

    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title = models.CharField(max_length=30)
    status = models.CharField(max_length=30, choices=TODO_STATUSES, default='N')
    users = models.ManyToManyField(get_user_model(), blank=True, related_name='shares')

    size = models.PositiveIntegerField(default=1)
    due = models.DateField(auto_now_add=True)
    due_time = models.DateTimeField(auto_now_add=True)
    object_ids = ArrayField(models.CharField(max_length=100), default=list, blank=True, null=True)

    def formatted_feed_message(self, verb):
        if verb == 'test':
            return "{} was tested".format(self.title)
        return None


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = '__all__'


"""
This is really an integration test
"""
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from streamio.streamio import StreamUser
from .models import Todo

from datetime import datetime, timedelta
import json

class StreamUserTestCase(TestCase):

    def setUp(self):
        self.user = StreamUser(1)
        self.djuser = get_user_model().objects.create(username='joe', password='testtest')
        self.todo = Todo.objects.create(owner=self.djuser, title='foo')

    def test_perform_action(self):
        res = self.user.perform_action(self.todo, "create")

    def test_get_notifications(self):
        res = self.user.get_notifications()

    def test_get_feed(self):
        res = self.user.get_feed()

class ActionMixinTestCase(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create(username='joe', password='testtest')

    def test_track_action(self):
        todo = Todo.objects.create(owner=self.user, title='foo')
        todo.track_action('test')

    def test_track_action_with_to_items(self):
        todo = Todo.objects.create(owner=self.user, title='foo')
        res = todo.track_action('test')

    def test_send_notification(self):
        todo = Todo.objects.create(owner=self.user, title='foo')
        res = todo.add_notification(
            "test",
            'this is a test notification',
            users_to_notify = [1],
            forward = {
                "sms": [ "+27832566533" ],
                "email": [ "tech@appointmentguru.co" ],
                "inapp": [ "practitioner.1" ],
            }
        )

    def test_custom_date_field(self):
        now = datetime.now()
        dates = [
            now - timedelta(days=2),
            now - timedelta(days=1),
        ]
        for dt in dates:
            todo = Todo.objects.create(owner=self.user, title='foo')
            todo.due_time = dt
            todo.save()
            activity = todo.track_action('due', date_field='due_time')

            self.assertEqual(todo.due_time, dt)
            self.assertEqual(
                todo.due_time,
                activity.get('activity').get('time')
            )

    def test_maps_object_ids_to_to(self):
        todo = Todo.objects.create(owner=self.user, title='foo')
        todo.object_ids = [
            "user:2",
            "user:2",
            "user:3",
            "user:4",
            "notification:5",
            "foo:6", # foo is mapped to todo in config
        ]
        todo.save()
        activity = todo.track_action('created')
        to_items = activity.get('activity').get('to')

        assert "user:2" in to_items, to_items
        assert "notification:5" in to_items, to_items
        assert "todo:6" in to_items, to_items


    def test_get_feed(self):
        todo = Todo.objects.create(owner=self.user, title='foo')
        res = todo.get_feed()

    def test_handles_m2m_fields(self):
        user1 = get_user_model().objects.create_user(username='u1', password='testtest')
        user2 = get_user_model().objects.create_user(username='u2', password='testtest')

        todo = Todo.objects.create(owner=self.user, title='foo')
        todo.users.add(user1)
        todo.users.add(user2)

        res = todo.track_action(verb='share')
        to = res.get('activity').get('to')

        user_1_key = "user:{}".format(user1.id)
        user_2_key = "user:{}".format(user2.id)

        assert user_1_key in to, \
            'Expected to find: {} in {}'.format(user_1_key, to)
        assert user_2_key in to, \
            'Expected to find: {} in {}'.format(user_2_key, to)

    def test_add_reaction(self):

        todo = Todo.objects.create(owner=self.user, title='foo')
        activity = todo.track_action(verb='create')
        activity_id = activity.get('activity').get('id')
        reaction = todo.add_reaction(
            "like",
            activity_id,
            data = {"text": "that's great"}
        )
        self.assertEqual(reaction.get('user_id'), str(todo.owner_id))

        reactions = todo.get_reactions(activity_id)
        self.assertEqual(
            reaction.get('id'),
            reactions.get('results')[0].get('id')
        )

    def test_add_extra_data(self):

        todo = Todo.objects.create(owner=self.user, title='foo')
        activity = todo.track_action(
            "with_extra_data",
            extra_data={"foo": {"baz": "bar"}}
        )
        self.assertEqual(
            activity.get('activity').get('foo'),
            {"baz": "bar"}
        )

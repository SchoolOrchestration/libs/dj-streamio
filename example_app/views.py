from django.shortcuts import render

from django.conf.urls import url, include
from .models import Todo, TodoSerializer
from rest_framework import routers, serializers, viewsets

# from streamio.viewsets import StreamViewSetMixin
from streamio.viewsets_legacy import StreamViewSetMixin

class TodoViewSet(StreamViewSetMixin, viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer

router = routers.DefaultRouter()
router.register(r'todos', TodoViewSet)

